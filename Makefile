DOCKER=docker
BUILDFLAGS=--rm

base:
	${DOCKER} build ${BUILDFLAGS} -t vguardiola/gentoo gentoo/
amd64:
	${DOCKER} build ${BUILDFLAGS} --no-cache -t vguardiola/gentoo-amd64 gentooScratch/
nginx:
	${DOCKER} build ${BUILDFLAGS} -t vguardiola/gentoo-nginx gentoo-nginx/
php72:
	${DOCKER} build ${BUILDFLAGS} -t vguardiola/gentoo-php:7.2 gentoo-php72/
php72d:
	${DOCKER} build ${BUILDFLAGS} -t vguardiola/gentoo-php:7.2-debug gentoo-php72-debug/
elastic:
	${DOCKER} build ${BUILDFLAGS} -t vguardiola/gentoo-elasticsearch gentoo-elasticsearch/
redis:
	${DOCKER} build ${BUILDFLAGS} -t vguardiola/gentoo-redis gentoo-redis/
smtp:
	${DOCKER} build ${BUILDFLAGS} -t vguardiola/gentoo-smtp gentoo-smtp/
ejabberd:
	${DOCKER} build ${BUILDFLAGS} -t vguardiola/gentoo-ejabberd gentoo-ejabberd/
dphp73:
	${DOCKER} build ${BUILDFLAGS} --no-cache -t vguardiola/debian-php:7.3 debian-php73/
dphp73d:
	${DOCKER} build ${BUILDFLAGS} --no-cache -t vguardiola/debian-php:7.3-debug debian-php73/ -f debian-php73/Dockerfile.dev
dphp73s:
	${DOCKER} build ${BUILDFLAGS} --no-cache -t vguardiola/debian-php:7.3-supervisor debian-php73/ -f debian-php73/Dockerfile.supervisor
dphp74:
	${DOCKER} build ${BUILDFLAGS} --no-cache -t vguardiola/debian-php:7.4 debian-php74/
dphp74d:
	${DOCKER} build ${BUILDFLAGS} --no-cache -t vguardiola/debian-php:7.4-debug debian-php74/ -f debian-php74/Dockerfile.dev
dphp74s:
	${DOCKER} build ${BUILDFLAGS} --no-cache -t vguardiola/debian-php:7.4-supervisor debian-php74/ -f debian-php74/Dockerfile.supervisor
all:
	make amd64
	make base
	make nginx
	make php72
	make redis
	make push
alld73:
	docker pull php:7.3-fpm
	make dphp73
	make dphp73d
alld74:
	docker pull php:7.4-fpm
	make dphp74
	make dphp74d
pushd73:
	${DOCKER} push vguardiola/debian-php:7.3
	${DOCKER} push vguardiola/debian-php:7.3-debug
	${DOCKER} push vguardiola/debian-php:7.3-supervisor
pushd74:
	${DOCKER} push vguardiola/debian-php:7.4
	${DOCKER} push vguardiola/debian-php:7.4-debug
	${DOCKER} push vguardiola/debian-php:7.4-supervisor
push:
	${DOCKER} push vguardiola/gentoo-amd64
	${DOCKER} push vguardiola/gentoo
	${DOCKER} push vguardiola/gentoo-nginx
	${DOCKER} push vguardiola/gentoo-php:7.2
	${DOCKER} push vguardiola/gentoo-php:7.2-debug
	${DOCKER} push vguardiola/gentoo-redis
pull:
	${DOCKER} pull vguardiola/gentoo-amd64
	${DOCKER} pull vguardiola/gentoo
	${DOCKER} pull vguardiola/gentoo-nginx
	${DOCKER} pull vguardiola/gentoo-redis
	${DOCKER} pull vguardiola/debian-php:7.2
	${DOCKER} pull vguardiola/debian-php:7.2-debug
	${DOCKER} pull vguardiola/debian-php:7.2-supervisor