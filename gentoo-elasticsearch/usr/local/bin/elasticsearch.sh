#!/usr/bin/env bash
ulimit -l unlimited
ES_USER="elasticsearch"
export ES_CLASSPATH="/usr/share/elasticsearch/lib/elasticsearch-1.7.5.jar:/usr/share/elasticsearch/lib/*:/usr/share/elasticsearch/lib/sigar/*"
PIDFILE="/run/elasticsearch/elasticsearch.pid"
ES_BASE_PATH="/var/lib/elasticsearch/_default"
ES_CONF_PATH="/etc/elasticsearch"
ES_LOG_PATH="/var/log/elasticsearch/_default"
ES_DATA_PATH="${ES_BASE_PATH}/data"
ES_WORK_PATH="${ES_BASE_PATH}/work"
export ES_INCLUDE="${ES_CONF_PATH}/elasticsearch.in.sh"
export JAVA_OPTS
export ES_JAVA_OPTS
export ES_HEAP_SIZE
export ES_HEAP_NEWSIZE
export ES_DIRECT_SIZE
export ES_USE_IPV4
/usr/share/elasticsearch/bin/elasticsearch -p ${PIDFILE} -Des.default.path.conf=\"${ES_CONF_PATH}\" -Des.default.path.data=\"${ES_DATA_PATH}\" -Des.default.path.work=\"${ES_WORK_PATH}\" -Des.default.path.logs=\"${ES_LOG_PATH}\"
